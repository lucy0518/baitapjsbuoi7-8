function minElement(array) {
  //Giả định vị trí số nhỏ nhất là số đầu tiên của mảng.
  var min = array[0];
  var min_index = 0;
  /*So sánh từng số trong mảng với giá trị đầu tiên để tìm ra giá trị nhỏ nhất*/
  for (var i = 1; i < array.length; ++i) {
    if (min > array[i]) {
      //Thay đổi giá trị nhỏ nhất nếu tìm ra số nhỏ hơn
      min = array[i];
      min_index = i;
    }
  }
  console.log("min= ", min);
  console.log("min_index= ", min_index);
}

var num = [5, 4, 7, 2, 8, 7, 3];
minElement(num);
